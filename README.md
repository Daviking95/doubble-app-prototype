# doubble-app-prototype
Doubble is an automated savings&#x2F;investment platform that allows you to save&#x2F;invest at your convenience and get back up to 100% returns over a period. With DOUBBLE, you choose to save&#x2F;invest either in one contribution (lump sum) or periodic contributions with options in both Naira and Dollars. You can also get your pay out monthly or lump sum based on the preferred savings variant. Doubble is powered by Sterling Bank

## App Distribution Link 
- https://appdistribution.firebase.dev/i/5f57676e308be9f6

## App Screenshots
| Screenshots  | Screenshots  | Screenshots |
| :------------ |:---------------:| -----:|
| ![dashboard-screen](/uploads/9d33c59dac9c755365a0bef7e464231b/dashboard-screen.png)      | ![earnings-screen](/uploads/a71db46a74d0b8e4fa71a9b3df08e10e/earnings-screen.png) | ![get-started-screen](/uploads/69e9ad90931007b5762a1a4e5531aa84/get-started-screen.png) |
| ![investment-screen](/uploads/018c9ba26822eb502d8a2b06fbaa0998/investment-screen.png)      | ![launch-screen](/uploads/ee40232d45e3fc89d30b815e3f18df9a/launch-screen.png)        | ![loading-screen](/uploads/02984f47451606c0d8675dbabacf8f3f/loading-screen.png)   |
| ![login-screen](/uploads/fe02fcb8052717ca05a2ac2fd73af95a/login-screen.png)      | ![profile-screen](/uploads/1fac4f3a11d610ea5c4ede9763544184/profile-screen.png)        | ![register-screen](/uploads/6396c92705adcd624f461bf4cf7e043c/register-screen.png)   |
| ![reward-screen](/uploads/cc194856c8383d8a0942b22c58490e43/reward-screen.png)      | ![start-screen](/uploads/7c3cec834748b2a09232705faaa888ba/start-screen.png)        |    |